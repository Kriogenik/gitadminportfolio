package router

import (
	"../api/group"
	"../api/middlewares"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func New() *echo.Echo {
	e := echo.New()

	//читабельные логи
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "[${time_rfc3339}] ${status} ${method} ${host}${uri} ${error}\n",
	}))

	e.Use(middleware.Recover())

	adminGroup := e.Group("/admin")
	middlewares.SetAdminMiddlewares(adminGroup)
	group.AdminGroup(adminGroup)

	return e
}
