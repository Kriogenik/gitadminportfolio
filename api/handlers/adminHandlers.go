package handlers

import (
	"log"
	"net/http"

	"../../db"
	"../../models"
	"../crypt"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

////////////////////// User //////////////////////////

// Отправляем пользователей на клиент
func GetUsers(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(*models.JwtCustomClaims)
	admin := claims.Admin

	if admin.Role != models.Administrator {
		return c.String(http.StatusInternalServerError, "Ошибочку надо будет написать, на случай говнеца")
	}

	db := db.Manager()

	users := []models.User{}

	db.Find(&users)

	return c.JSON(http.StatusOK, users)
}

// Нужно как-то сделать так, чтобы анкеты тоже с ним отправлялись, надо подумать как
func GetUser(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(*models.JwtCustomClaims)
	admin := claims.Admin

	if admin.Role != models.Administrator {
		return c.String(http.StatusInternalServerError, "Ошибочку надо будет написать, на случай говнеца")
	}

	db := db.Manager()

	user := models.User{}

	id := c.Param("id")

	db.Preload("Questionnaires.Photos", "Type = ?", models.MainPhoto).First(&user, id)

	return c.JSON(http.StatusOK, user)
}

////////////////////// User //////////////////////////

////////////////////// Questionnaire //////////////////////////

// Отправляем все существующие анкеты, прошедшие модерацию
func GetQuestionnaires(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(*models.JwtCustomClaims)
	admin := claims.Admin

	if admin.Role != models.Administrator {
		return c.String(http.StatusInternalServerError, "Ошибочку надо будет написать, на случай говнеца")
	}

	db := db.Manager()

	questionnaires := []models.Questionnaire{}

	db.Where(&models.Questionnaire{Moderation: true}).Preload("Photos", "Type = ?", models.MainPhoto).Find(&questionnaires)

	return c.JSON(http.StatusOK, questionnaires)

}

// Отправляем нужную анкету админу
func GetQuestionnaire(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(*models.JwtCustomClaims)
	admin := claims.Admin

	if admin.Role != models.Administrator {
		return c.String(http.StatusInternalServerError, "Ошибочку надо будет написать, на случай говнеца")
	}

	db := db.Manager()

	questionnaire := models.Questionnaire{}

	id := c.Param("id")

	db.Preload("Photos").First(&questionnaire, id)

	return c.JSON(http.StatusOK, questionnaire)
}

////////////////////// Questionnaire //////////////////////////

//

////////////////////// SERVICE //////////////////////////

func AddService(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(*models.JwtCustomClaims)
	admin := claims.Admin

	if admin.Role != models.Administrator {
		return c.String(http.StatusInternalServerError, "Ошибочку надо будет написать, на случай говнеца")
	}

	db := db.Manager()

	filter := models.Filter{}

	if err := c.Bind(&filter); err != nil {
		return err
	}

	filter.Status = true

	db.Create(&filter)

	return c.JSON(http.StatusOK, filter)
}

func NewcStatus(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(*models.JwtCustomClaims)
	admin := claims.Admin

	if admin.Role != models.Administrator {
		return c.String(http.StatusInternalServerError, "Ошибочку надо будет написать, на случай говнеца")
	}

	db := db.Manager()

	filter := models.Filter{}

	id := c.Param("id")

	db.First(&filter, id)

	filter.Status = !filter.Status

	return c.JSON(http.StatusOK, filter)

}

////////////////////// SERVICE //////////////////////////

//

////////////////////// Moderation //////////////////////////

// Отправляем все существующие анкеты, которые не прошли модерацию на клиент админки
func GetQuestionnairesModeration(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(*models.JwtCustomClaims)
	admin := claims.Admin

	if admin.Role != models.Administrator {
		return c.String(http.StatusInternalServerError, "Ошибочку надо будет написать, на случай говнеца")
	}

	db := db.Manager()

	questionnaires := []models.Questionnaire{}

	db.Where(&models.Questionnaire{Moderation: false}).Preload("Photos", "Type = ?", models.MainPhoto).Find(&questionnaires)

	return c.JSON(http.StatusOK, questionnaires)
}

//

func UpModeration(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(*models.JwtCustomClaims)
	admin := claims.Admin

	if admin.Role != models.Administrator {
		return c.String(http.StatusInternalServerError, "Ошибочку надо будет написать, на случай говнеца")
	}

	db := db.Manager()

	questionnaire := models.Questionnaire{}

	id := c.Param("id")

	db.First(&questionnaire, id)

	questionnaire.Moderation = !questionnaire.Moderation

	db.Update(&questionnaire)

	return c.JSON(http.StatusOK, questionnaire)

}

func HidePhoto(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(*models.JwtCustomClaims)
	admin := claims.Admin

	if admin.Role != models.Administrator {
		return c.String(http.StatusInternalServerError, "Ошибочку надо будет написать, на случай говнеца")
	}

	db := db.Manager()

	photo := models.Photo{}

	id := c.Param("id")

	db.First(&photo, id)

	photo.Status = !photo.Status

	return c.JSON(http.StatusOK, photo)
}

////////////////////// Moderation //////////////////////////

func Login(c echo.Context) error {
	login := c.FormValue("username")
	password := c.FormValue("password")
	//хеширование пароля
	password = crypt.Hashing(login, password)

	db := db.Manager()

	admin := models.Admin{}

	db.Where(&models.Admin{Login: login}).First(&admin)

	if admin.Password == password {
		token, err := crypt.CreateJwtToken(admin)
		if err != nil {
			log.Println("Error Creating JWT token", err)
			return c.String(http.StatusInternalServerError, "something went wrong")
		}

		return c.JSON(http.StatusOK, echo.Map{
			"token": token,
		})
	}

	return c.String(http.StatusUnauthorized, "Your username or password were wrong")
}
