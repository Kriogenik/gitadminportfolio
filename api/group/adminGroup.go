package group

import (
	"../handlers"

	"github.com/labstack/echo"
)

// Здесь происходит роутинг функция админа
func AdminGroup(g *echo.Group) {

	// Отправки
	g.GET("/Users", handlers.GetUsers)
	g.GET("/User", handlers.GetUser)
	g.GET("/Questionnaires", handlers.GetQuestionnaires)
	g.GET("/Questionnaire", handlers.GetQuestionnaire)
	g.GET("/QuestionnairesModeration", handlers.GetQuestionnairesModeration)

	// Обновление
	g.PUT("/UpModeration", handlers.UpModeration)

	// Добавление
	g.POST("/AddService", handlers.AddService)

}
