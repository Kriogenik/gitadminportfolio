package crypt

import (
	"time"

	"../../models"
	jwt "github.com/dgrijalva/jwt-go"
)

func CreateJwtToken(admin models.Admin) (string, error) {
	claims := &models.JwtCustomClaims{
		admin,
		jwt.StandardClaims{
			// Id:        string(user.ID),
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
		},
	}

	rawToken := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)

	token, err := rawToken.SignedString([]byte("IceIceBaby"))
	if err != nil {
		return "", err
	}

	return token, nil
}
