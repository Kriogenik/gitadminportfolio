package models

import "github.com/jinzhu/gorm"

type Role int

const (
	Administrator Role = 0
	// Moderator     Role = 1
)

type Admin struct {
	gorm.Model
	Login    string
	Password string
	Role     Role
}
