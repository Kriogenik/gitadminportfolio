package models

import "github.com/jinzhu/gorm"

// Метро, для указания в анкетах
type Metro struct {
	gorm.Model
	Name           string
	Questionnaires []Questionnaire `gorm:"many2many:questionnaire_metro;"`
}
