package models

import "github.com/jinzhu/gorm"

type TypePhoto int

const (
	UsualPhoto TypePhoto = 0
	MainPhoto  TypePhoto = 1
)

type Photo struct {
	gorm.Model
	Path            string
	Status          bool
	QuestionnaireID uint
	Type            TypePhoto
}
